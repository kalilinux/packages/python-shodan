Source: python-shodan
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Devon Kearns <dookie@kali.org>
Build-Depends: debhelper (>= 11), dh-python, python-all, python-setuptools, python3-all, python3-setuptools, python3-sphinx
Standards-Version: 4.1.4
Homepage: https://github.com/achillean/shodan-python
Vcs-Git: git://git.kali.org/packages/python-shodan.git
Vcs-Browser: http://git.kali.org/gitweb?p=packages/python-shodan.git;a=summary

Package: python-shodan
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}
Suggests: python-shodan-doc
Description: Official Python library for Shodan (Python 2)
 Shodan is a search engine for Internet-connected devices. Google lets you
 search for websites, Shodan lets you search for devices. This library provides
 developers easy access to all of the data stored in Shodan in order to
 automate tasks and integrate into existing tools.
 .
 This package installs the library for Python 2.

Package: python3-shodan
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Suggests: python-shodan-doc
Description: Official Python library for Shodan (Python 3)
 Shodan is a search engine for Internet-connected devices. Google lets you
 search for websites, Shodan lets you search for devices. This library provides
 developers easy access to all of the data stored in Shodan in order to
 automate tasks and integrate into existing tools.
 .
 This package installs the library for Python 3.

Package: python-shodan-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: Official Python library for Shodan (common documentation)
 Shodan is a search engine for Internet-connected devices. Google lets you
 search for websites, Shodan lets you search for devices. This library provides
 developers easy access to all of the data stored in Shodan in order to
 automate tasks and integrate into existing tools.
 .
 This package installs the common documentation package.
